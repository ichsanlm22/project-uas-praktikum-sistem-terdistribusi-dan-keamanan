<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Daftar2 extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nik' => $this->nik,
            'nama' => $this->nama,
            'alamat' => $this->alamat,
            'lahir' => $this->lahir,
            'gender' => $this->gender,
            'user_id' => $this->user_id,
            'beasiswaid' => $this->beasiswaid,
        ];
    }
}