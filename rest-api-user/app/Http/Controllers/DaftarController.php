<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Daftar;
use App\Http\Resources\Daftar as DaftarResource;

class DaftarController extends BaseController
{
    public function index()
    {
        $daftar = Daftar::all();
        return $this->sendResponse(DaftarResource::collection($daftar), 'Daftar ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nik' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'lahir' => 'required',
            'gender' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $daftar = new Daftar();
        $daftar->nik = $input['nik'];
        $daftar->nama = $input['nama'];
        $daftar->alamat = $input['alamat'];
        $daftar->lahir = $input['lahir'];
        $daftar->gender = $input['gender'];
        $daftar->user_id = $input['user_id'];
        $daftar->save();

        return $this->sendResponse(new DaftarResource($daftar), 'Data Daftar ditambahkan.');
    }

    public function show($id)
    {
        $daftar = Daftar::find($id);
        if (is_null($daftar)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new DaftarResource($daftar), 'Data ditampilkan.');
    }

    public function update(Request $request, Daftar $daftar)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'nik' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'lahir' => 'required',
            'gender' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }

        $daftar->nik = $input['nik'];
        $daftar->nama = $input['nama'];
        $daftar->alamat = $input['alamat'];
        $daftar->lahir = $input['lahir'];
        $daftar->gender = $input['gender'];
        $daftar->user_id = $input['user_id'];
        $daftar->save();

        return $this->sendResponse(new DaftarResource($daftar), 'Data updated.');
    }

    public function destroy(Daftar $daftar)
    {
        $daftar->delete();

        return $this->sendResponse([], 'Data deleted.');
    }
}