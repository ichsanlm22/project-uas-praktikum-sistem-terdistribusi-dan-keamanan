<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\Daftar2Controller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'signin']);
Route::post('register', [AuthController::class, 'signup']);

// Route::middleware('auth:sanctum')->group(function () {
//     Route::resource('Daftar2', Daftar2Controller::class);
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('daftar2', [Daftar2Controller::class, 'index']);
Route::post('daftar2', [Daftar2Controller::class, 'create']);
Route::put('/daftar2/{id}', [Daftar2Controller::class, 'update']);
Route::delete('daftar2/{id}', [Daftar2Controller::class, 'destroy']);
Route::get('daftar2/{id}', [Daftar2Controller::class, 'show']);