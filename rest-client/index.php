<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Home</title>

    <style>
    * {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
    }

    .banner {
        width: 100%;
        height: 100vh;
        background-image: linear-gradient(rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0.75)), url('tampilan/img/home.jpg');
        background-size: cover;
        background-position: center;
    }

    .content {
        width: 100%;
        position: absolute;
        top: 50%;
        transform: translateY(10%);
        text-align: center;
        color: white;
    }

    .content h1 {
        font-size: 96px;
        margin-top: 100px;
        font-weight: bold;
    }

    .content p {
        margin: 20px auto;
        font-weight: 100;
        line-height: 25px;
    }

    button {
        width: 200px;
        padding: 15px;
        text-align: center;
        margin: 20px 30px;
        border-radius: 30px;
        font-weight: bold;
        border: 1px solid #4e73df;
        background: transparent;
        color: white;
        cursor: pointer;
        position: relative;
        overflow: hidden;
    }

    hr {
        color: white;
        margin-left: 450px;
        margin-right: 450px;
    }

    span {
        background: #4e73df;
        height: 100%;
        width: 0;
        border-radius: 30px;
        position: absolute;
        left: 0;
        bottom: 0;
        z-index: -1;
        transition: 0.3s;
    }

    button:hover span {
        width: 100%;
    }

    button:hover {
        border: none;
    }
    </style>
</head>

<body>
    <div class="banner">
        <div class="navbar">
            <div class="content">
                <h1>Ayo Ikut Beasiswa!</h1>
                <p>Raih cita-citamu dan gapai impianmu , <br> dan daftar pada web kami untuk info beasiswa menarik.</p>
                <div>
                    <hr>
                    <button type="button" onclick="window.location.href='login_admin.php'"><span></span>Login as
                        Admin</button>
                    <button type="button" onclick="window.location.href='login_user.php'"><span></span>Login as
                        User</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>