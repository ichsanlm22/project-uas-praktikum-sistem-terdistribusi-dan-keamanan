<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeasiswaClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beasiswaclients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('beasiswa_id')->unsigned();
            $table->integer('lembaga_id')->unsigned();
            $table->timestamps();

            // $table->foreign('vaksin_id')->references('id')->on('vaksins')->onDelete('CASCADE');
            // $table->foreign('rs_id')->references('id')->on('rumah_sakits')->onDelete('CASCADE');
        });

        // Schema::table('priorities', function ($table) {
        //     $table->foreign('vaksin_id')->references('id')->on('vaksins')->onDelete('CASCADE');
        //     $table->foreign('rs_id')->references('id')->on('rumah_sakits')->onDelete('CASCADE');
        // });

        Schema::table('beasiswaclients', function ($table) {
            $table->foreign('beasiswa_id')->references('id')->on('beasiswas');
            $table->foreign('lembaga_id')->references('id')->on('lembagas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beasiswaclients');
    }
}