<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\BeasiswaController;
use App\Http\Controllers\LembagaController;
use App\Http\Controllers\BeasiswaClientController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [AuthController::class, 'signin']);
Route::post('register', [AuthController::class, 'signup']);

// Route::middleware('auth:sanctum')->group(function () {
//     Route::resource('vaksin', VaksinController::class);
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('beasiswa', [BeasiswaController::class, 'index']);
Route::post('beasiswa', [BeasiswaController::class, 'create']);
Route::put('/beasiswa/{id}', [BeasiswaController::class, 'update']);
Route::delete('beasiswa/{id}', [BeasiswaController::class, 'destroy']);
Route::get('beasiswa/{id}', [BeasiswaController::class, 'show']);

Route::get('lembaga', [LembagaController::class, 'index']);
Route::post('lembaga', [LembagaController::class, 'create']);
Route::put('/lembaga/{id}', [LembagaController::class, 'update']);
Route::delete('lembaga/{id}', [LembagaController::class, 'destroy']);
Route::get('lembaga/{id}', [LembagaController::class, 'show']);

Route::get('beasiswaclient', [BeasiswaClientController::class, 'index']);
Route::post('beasiswaclient', [BeasiswaClientController::class, 'create']);
Route::put('/beasiswaclient/{id}', [BeasiswaClientController::class, 'update']);
Route::delete('beasiswaclient/{id}', [BeasiswaClientController::class, 'destroy']);
Route::get('beasiswaclient/{id}', [BeasiswaClientController::class, 'show']);