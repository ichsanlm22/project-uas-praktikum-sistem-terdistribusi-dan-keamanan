<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\BeasiswaClient;
use App\Http\Resources\BeasiswaClient as BeasiswaClientResource;

class BeasiswaClientController extends BaseController
{
    public function index()
    {
        $beasiswaclient = BeasiswaClient::all();
        return $this->sendResponse(BeasiswaClientResource::collection($beasiswaclient), 'Beasiswa ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'beasiswa_id' => 'required',
            'lembaga_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $beasiswaclient = new BeasiswaClient();
        $beasiswaclient->beasiswa_id = $input['beasiswa_id'];
        $beasiswaclient->lembaga_id = $input['lembaga_id'];
        $beasiswaclient->save();

        return $this->sendResponse(new BeasiswaClientResource($beasiswaclient), 'Data Beasiswa ditambahkan.');
    }

    public function show($id)
    {
        $beasiswaclient = BeasiswaClient::find($id);
        if (is_null($beasiswaclient)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new BeasiswaClientResource($beasiswaclient), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $beasiswaclient = BeasiswaClient::find($id);
        if (!is_null($beasiswaclient)) {
            $validator = Validator::make($input, [
                'beasiswa_id' => 'required',
                'lembaga_id' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $beasiswaclient->beasiswa_id = $input['beasiswa_id'];
            $beasiswaclient->lembaga_id = $input['lembaga_id'];
            $beasiswaclient->save();
        }

        return $this->sendResponse(new BeasiswaClientResource($beasiswaclient), 'Data updated.');
    }

    public function destroy($id)
    {
        $beasiswaclient = BeasiswaClient::find($id);
        if (!is_null($beasiswaclient)) {
            $beasiswaclient->delete();
        }
        // $beasiswaclient->delete();

        return $this->sendResponse([], 'Data deleted.');
    }
}