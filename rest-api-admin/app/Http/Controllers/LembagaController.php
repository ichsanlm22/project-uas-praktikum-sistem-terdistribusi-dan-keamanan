<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Lembaga;
use App\Http\Resources\Lembaga as LembagaResource;

class LembagaController extends BaseController
{
    public function index()
    {
        $lembaga = Lembaga::all();
        return $this->sendResponse(LembagaResource::collection($lembaga), 'Lembaga ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nama' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $lembaga = new Lembaga();
        $lembaga->nama = $input['nama'];
        $lembaga->save();

        return $this->sendResponse(new LembagaResource($lembaga), 'Data Lembaga ditambahkan.');
    }

    public function show($id)
    {
        $lembaga = Lembaga::find($id);
        if (is_null($lembaga)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new LembagaResource($lembaga), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $lembaga = Lembaga::find($id);
        if (!is_null($lembaga)) {
            $validator = Validator::make($input, [
                'nama' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $lembaga->nama = $input['nama'];
            $lembaga->save();
        }

        return $this->sendResponse(new LembagaResource($lembaga), 'Data updated.');
    }

    public function destroy($id)
    {
        $lembaga = Lembaga::find($id);
        if (!is_null($lembaga)) {
            $lembaga->delete();
        }

        return $this->sendResponse([], 'Data deleted.');
    }
}