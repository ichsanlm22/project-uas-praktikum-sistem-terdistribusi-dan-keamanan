<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Beasiswa;
use App\Http\Resources\Beasiswa as BeasiswaResource;

class BeasiswaController extends BaseController
{
    public function index()
    {
        $beasiswa = Beasiswa::all();
        return $this->sendResponse(BeasiswaResource::collection($beasiswa), 'Beasiswa ditampilkan.');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nama' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error validation', $validator->errors());
        }

        $beasiswa = new Beasiswa();
        $beasiswa->nama = $input['nama'];
        $beasiswa->save();

        return $this->sendResponse(new BeasiswaResource($beasiswa), 'Data Beasiswa ditambahkan.');
    }

    public function show($id)
    {
        $beasiswa = Beasiswa::find($id);
        if (is_null($beasiswa)) {
            return $this->sendError('Data does not exist.');
        }
        return $this->sendResponse(new BeasiswaResource($beasiswa), 'Data ditampilkan.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $lembaga = Beasiswa::find($id);
        if (!is_null($lembaga)) {
            $validator = Validator::make($input, [
                'nama' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->errors());
            }

            $lembaga->nama = $input['nama'];
            $lembaga->save();
        }

        return $this->sendResponse(new BeasiswaResource($lembaga), 'Data updated.');
    }

    public function destroy($id)
    {
        $beasiswa = Beasiswa::find($id);
        if (!is_null($beasiswa)) {
            $beasiswa->delete();
        }

        return $this->sendResponse([], 'Data deleted.');
    }
}